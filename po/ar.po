# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the PACKAGE package.
# 
# Translators:
# Asim Jaweesh <asim.abdo_20@hotmail.com>, 2013
# Mayalynn <mayalynn@live.com>, 2014
# Sherief Alaa <sheriefalaa.w@gmail.com>, 2013
# محيي الدين <tx99h4@hotmail.com>, 2014
msgid ""
msgstr ""
"Project-Id-Version: The Tor Project\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2015-12-16 15:03+0100\n"
"PO-Revision-Date: 2015-12-17 09:29+0000\n"
"Last-Translator: carolyn <carolyn@anhalt.org>\n"
"Language-Team: Arabic (http://www.transifex.com/otf/torproject/language/ar/)\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Language: ar\n"
"Plural-Forms: nplurals=6; plural=n==0 ? 0 : n==1 ? 1 : n==2 ? 2 : n%100>=3 && n%100<=10 ? 3 : n%100>=11 && n%100<=99 ? 4 : 5;\n"

#: mat-gui:64 mat-gui:415 mat-gui:438
msgid "Ready"
msgstr "جاهز"

#: mat-gui:133
msgid "Choose files"
msgstr "اختر الملفات"

#: mat-gui:141
msgid "All files"
msgstr "كل الملفات"

#: mat-gui:147
msgid "Supported files"
msgstr "الملفات المدعومة"

#: mat-gui:164 mat-gui:359 mat-gui:410 mat-gui:434 mat-gui:436
#: data/mat.glade:480
msgid "Clean"
msgstr "نظف"

#: mat-gui:165
msgid "No metadata found"
msgstr "لم يتم العثور علي بيانات وصفية"

#: mat-gui:167 mat-gui:412
msgid "Dirty"
msgstr "قذر"

#: mat-gui:172
#, python-format
msgid "%s's metadata"
msgstr "البيانات الوصفية الخاصة بـ%s"

#: mat-gui:183
msgid "Trash your meta, keep your data"
msgstr "الغاء بياناتك الوصفية و احتفظ بالبيانات العادية"

#: mat-gui:188
msgid "Website"
msgstr "موقع"

#: mat-gui:214
msgid "Preferences"
msgstr "التفضيلات"

#: mat-gui:227
msgid "Reduce PDF quality"
msgstr "قلل جودة ملف الـPDF"

#: mat-gui:230
msgid "Reduce the produced PDF size and quality"
msgstr "قلل جودة وحجم ملف الـPDF المنتج"

#: mat-gui:233
msgid "Add unsupported file to archives"
msgstr "اضافة ملف غير مدعوم للارشيف"

#: mat-gui:236
msgid "Add non-supported (and so non-anonymised) file to output archive"
msgstr "اضافة ملف غير مدعوم ( او ملف غير مجهول ) للارشيف الناتج"

#: mat-gui:275
msgid "Unknown"
msgstr "مجهول"

#: mat-gui:318
msgid "Not-supported"
msgstr "غير مدعوم"

#: mat-gui:332
msgid "Harmless fileformat"
msgstr "صيغة الملف غير مؤذية"

#: mat-gui:334
msgid "Cant read file"
msgstr ""

#: mat-gui:336
msgid "Fileformat not supported"
msgstr "صيغة الملف غير مدعمة"

#: mat-gui:339
msgid "These files can not be processed:"
msgstr "لا يمكن معالجة هذه الملفات:"

#: mat-gui:344 mat-gui:373 data/mat.glade:519
msgid "Filename"
msgstr "اسم الملف"

#: mat-gui:346
msgid "Reason"
msgstr "السبب"

#: mat-gui:358
msgid "Non-supported files in archive"
msgstr "الملفات في الأرشيف غير مدعمة"

#: mat-gui:372
msgid "Include"
msgstr "أدمِج"

#: mat-gui:390
#, python-format
msgid "MAT is not able to clean the following files, found in the %s archive"
msgstr "لم يستطع MAT تنظيف الملفات التالية الموجودة في الأرشيف %s"

#: mat-gui:406
#, python-format
msgid "Checking %s"
msgstr "جاري فحص %s"

#: mat-gui:421
#, python-format
msgid "Cleaning %s"
msgstr "جاري تنظيف %s"

#: data/mat.glade:26 data/mat.glade:196
msgid "Metadata"
msgstr "البيانات الوصفية"

#: data/mat.glade:85
msgid "Name"
msgstr "اسم"

#: data/mat.glade:99
msgid "Content"
msgstr "المحتوي"

#: data/mat.glade:129
msgid "Supported formats"
msgstr "الصيغ المدعومة"

#: data/mat.glade:185
msgid "Support"
msgstr "الدعم"

#: data/mat.glade:207
msgid "Method"
msgstr "طريقة"

#: data/mat.glade:218
msgid "Remaining"
msgstr "متبقي"

#: data/mat.glade:247
msgid "Fileformat"
msgstr "نوع الملف"

#: data/mat.glade:326
msgid "_File"
msgstr "_File"

#: data/mat.glade:375
msgid "_Edit"
msgstr "_Edit"

#: data/mat.glade:421
msgid "_Help"
msgstr "_Help"

#: data/mat.glade:467
msgid "Add"
msgstr "إضافة"

#: data/mat.glade:536
msgid "State"
msgstr "الحالة"
